# Annual report miscweb static websites

Blubber images for old annual report static html sites (2015-2017).

Mirror of https://gerrit.wikimedia.org/r/plugins/gitiles/wikimedia/annualreport/

## Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/2014/`


## Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/annualreport:<timestamp>`
